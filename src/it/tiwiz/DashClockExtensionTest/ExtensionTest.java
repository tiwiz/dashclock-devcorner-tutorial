package it.tiwiz.DashClockExtensionTest;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import com.google.android.apps.dashclock.api.DashClockExtension;
import com.google.android.apps.dashclock.api.ExtensionData;


public class ExtensionTest extends DashClockExtension
{
    private final static String awPackageName = "it.androidworld.app";
    private final static String playStoreIntentString = "market://details?id=" + awPackageName;

    protected void onInitialize(boolean isReconnect){
        //Diciamo a DashClock di aggiornarsi ogni volta che si accende lo schermo
        setUpdateWhenScreenOn(true);
    }

    @Override
    protected void onUpdateData(int i)
    {
        Intent extensionClickIntent;
        String title = getString(R.string.extension_title);
        String message = "";

        if(isInstalled()){
            if(isOnline())
                message = getConnectedString();
            else
                message = getString(R.string.extension_message_offline);
            extensionClickIntent = getPackageManager().getLaunchIntentForPackage(awPackageName);
        }else{
            extensionClickIntent = new Intent(Intent.ACTION_VIEW);
            extensionClickIntent.setData(Uri.parse(playStoreIntentString));
            message = getString(R.string.extension_message_false);
        }

        //Creiamo l'estensione
        ExtensionData extensionData = new ExtensionData();
        extensionData.visible(true); //rendiamo visibile la nostra estensione
        extensionData.icon(R.drawable.ic_extension_test); //Impostiamo l'icona
        extensionData.status("AW.it"); //impostiamo lo status che viene mostrato quando DashClock non è massimizzato
        extensionData.expandedTitle(title); //impostiamo il titolo della notifica espansa
        extensionData.expandedBody(message); //e il messaggio
        extensionData.clickIntent(extensionClickIntent); //impostiamo l'azione da compiere al click

        //e diciamo a DashClock di visualizzare la nostra estensione
        publishUpdate(extensionData);

    }

    private boolean isInstalled(){
        boolean installed = true;

        PackageManager pm = getPackageManager();

        //cerchiamo informazioni circa il package di AndroidWorld.it, se non c'è, finisce nel catch e capiamo che non è installato
        try{
            PackageInfo info = pm.getPackageInfo(awPackageName,PackageManager.GET_META_DATA);
        }catch(PackageManager.NameNotFoundException e){
            installed = false;
        }

        return installed;
    }

    private boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        if(cm.getActiveNetworkInfo() != null)
            return cm.getActiveNetworkInfo().isConnectedOrConnecting();
        else
            return false;
    }

    private String getConnectedString(){

        String message = "";

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        //richiede permesso ACCESS_WIFI_STATE
        if(ni.getType() == ConnectivityManager.TYPE_WIFI){
            WifiManager wm = (WifiManager)getSystemService(WIFI_SERVICE);
            WifiInfo wi = wm.getConnectionInfo();
            message = getString(R.string.extension_message_online, ni.getTypeName(), wi.getSSID());
        }else if(ni.getType() == ConnectivityManager.TYPE_MOBILE){
            TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
            message = getString(R.string.extension_message_online, ni.getTypeName(), tm.getNetworkOperatorName());
        }else{
            message = getString(R.string.extension_message_online_unknown);
        }

        return message;
    }
}
